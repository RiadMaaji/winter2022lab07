public class Board{
	private Die dieOne;
	private Die dieTwo;
	private int[][] closedTiles;
	final int GAMEOVER = 3;

	public Board(){
		this.dieOne = new Die();
		this.dieTwo = new Die();
		this.closedTiles = new int[6][6];
	}
	public String toString(){
		String st = "";
		for(int i = 0; i < closedTiles.length; i++){
			for(int j = 0; j < closedTiles[i].length; j++){
			st = st + closedTiles[i][j];
			}
			st = st + " ";
			
		}
		return st;
	}
	public boolean playATurn(){
		this.dieOne.roll();
		this.dieTwo.roll();
		System.out.println(this.dieOne);
		System.out.println(this.dieTwo);
		if(closedTiles[dieOne.getPips()-1][dieTwo.getPips()-1]==GAMEOVER){
			return true;
			}
		else{
		  closedTiles[dieOne.getPips()-1][dieTwo.getPips()-1] = closedTiles[dieOne.getPips()-1][dieTwo.getPips()-1]+1;
		  return false;
		}
	}
}